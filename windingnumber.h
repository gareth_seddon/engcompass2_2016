//
// Created by Gareth on 29/12/2016.
//

#ifndef ENGCOMPASSIGNMENT_WINDINGNUMBER_H
#define ENGCOMPASSIGNMENT_WINDINGNUMBER_H

#include "structs.h"

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 and P1
//            =0 for P2  on the line
//            <0 for P2  right of the line
//    See: Algorithm 1 "Area of Triangles and Polygons"
int isLeft(point_t P0, point_t P1, point_t P2);

// crossing_number(): crossing number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  0 = outside, 1 = inside
// This code is patterned after [Franklin, 2000]
int crossing_number(point_t P, point_t *V, int n);


// winding_number(): winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)
int winding_number(point_t P, point_t *V, int n);

#endif //ENGCOMPASSIGNMENT_WINDINGNUMBER_H
