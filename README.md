# Engineering Computation Assignment 2
## Semester 2 2016

### Building
To build this, we use CMake to generate the makefile. Clone the repository, and open a command line at the folder. Then run
```
mkdir cmake
cd cmake
cmake ..
make
``` 
this will build the project into an executable called EngCompAssignment. This is tested working on Windows 10 under Cygwin, bash on Windows Subsystem for Linux and MacOS El Capitan.

### Usage

This is a command line tool. To use it, you can have either give it the input via stdin or as a command line argument. E.g. if you run the executable from where it is built in the cmake folder, you can run `./EngCompAssignment < ../waps.txt` or `./EngCompAssignment ../waps.txt`