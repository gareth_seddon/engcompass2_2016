//
// Created by Gareth on 28/12/2016.
//

#ifndef ENGCOMPASSIGNMENT_WAP_H
#define ENGCOMPASSIGNMENT_WAP_H

typedef struct {
    double x;
    double y;
} point_t;

typedef struct {
    point_t location;
    double power;
    double frequency;
} wap_t;

#endif //ENGCOMPASSIGNMENT_WAP_H
