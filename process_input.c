//
// Created by Gareth on 28/12/2016.
//

#include "process_input.h"

/**
 * will parse the provided file pointer into a list of wap and obs_points structs.
 */
void parse_input(FILE *fp, wap_t *waps, point_t *obs_points, point_t *polygon_verts, int *AP_count, int *point_count,
                 int *vert_count) {
    if (!fp) {
        fp = fopen("waps.txt", "r");
    }
    char line_buffer[LINE_MAX];
    const char token_delimiter[]  = " ";
    const char wap_identifier[]   = "W";
    const char point_identifier[] = "P";
    const char poly_identifier[]  = "V";
    char* token;

    while(fgets(line_buffer, LINE_MAX, fp)) {
        token = strtok(line_buffer, token_delimiter);
        assert(token);
        if (strcmp(token, wap_identifier) == STRCMP_EQUAL) {
            waps[*AP_count] = parse_wap(token_delimiter, token);
            (*AP_count)++;
        } else if (strcmp(token, point_identifier) == STRCMP_EQUAL) {
            obs_points[*point_count] = parse_point(token_delimiter, token);
            (*point_count)++;
        } else if (strcmp(token, poly_identifier) == STRCMP_EQUAL) {
            polygon_verts[*vert_count] = parse_point(token_delimiter, token);
            (*vert_count)++;
        }
    }
}

/**
 * Parse a line of text into an obs_point. The line must be formatted as: P 10.1 3.0
 * @param delimiter the delimiter the token should be split at. Typically " "
 * @param token a token pointing to the line, as created by the strtok function
 * @return
 */
point_t parse_point(const char delimiter[], char *token) {
    point_t point;
    token = strtok(NULL, delimiter);
    point.x = atof(token);
    token = strtok(NULL, delimiter);
    point.y = atof(token);
    return point;
}

/**
 * Parse a line of text into a wap struct. The line must be formatted the following way: W 10.1 0.1 30.0 5.0
 * @param delimiter the delimiter the token should be split at. Typically " "
 * @param token a token pointing to the line, as created by the strtok function
 * @return
 */
wap_t parse_wap(const char *delimiter, char *token) {
    wap_t new_wap;
    int i;
    double *wap_values[] = {&(new_wap.location.x), &(new_wap.location.y), &(new_wap.power), &(new_wap.frequency)};
    for (i = 0; i < count_of(wap_values); i++) {
            token = strtok(NULL, delimiter);
            *wap_values[i] = atof(token);
        }
    return new_wap;
}