//
// Created by Gareth on 28/12/2016.
//

#include "structs.h"
#include <math.h>

#ifndef ENGCOMPASSIGNMENT_POWER_H
#define ENGCOMPASSIGNMENT_POWER_H

/**
 * Uses pythagoras's formula to calculate distance between two points. c^2 = a^2 + b^2. Returns c^2 for performance reasons.
 * @param point1
 * @param point2
 * @return
 */
double distance_between_squared(point_t point1, point_t point2);

/**
 * will return the transmission power lost between a wap and a certain point.
 * @param point
 * @param wap
 * @return
 */
double received_power(point_t point, wap_t wap);

/**
 * given a point on a grid, and a list of WAPs, it will return the maximum power the point receives from any given WAP on the list.
 * @param waps the list of waps
 * @param wap_count the number of waps
 * @param point the point struct
 * @return
 */
double max_power_at_point(const wap_t *waps, int wap_count, point_t point) ;


#endif //ENGCOMPASSIGNMENT_POWER_H
