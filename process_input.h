//
// Created by Gareth on 28/12/2016.
//

#ifndef ENGCOMPASSIGNMENT_PROCESS_INPUT_H
#define ENGCOMPASSIGNMENT_PROCESS_INPUT_H

#define LINE_MAX 40
#define STRCMP_EQUAL 0
#define count_of(arr) sizeof(arr) / sizeof(arr[0])

#include "structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <assert.h>

/**
 * will parse the provided file pointer into a list of wap and obs_points structs.
 */
void parse_input(FILE *fp, wap_t *waps, point_t *obs_points, point_t *polygon_verts, int *AP_count, int *point_count,
                 int *vert_count);

/**
 * Parse a line of text into an obs_point. The line must be formatted as: P 10.1 3.0
 * @param delimiter the delimiter the token should be split at. Typically " "
 * @param token a token pointing to the line, as created by the strtok function
 * @return
 */
point_t parse_point(const char delimiter[], char *token);

/**
 * Parse a line of text into a wap struct. The line must be formatted the following way: W 10.1 0.1 30.0 5.0
 * @param delimiter the delimiter the token should be split at. Typically " "
 * @param token a token pointing to the line, as created by the strtok function
 * @return
 */
wap_t parse_wap(const char *delimiter, char *token);

#endif //ENGCOMPASSIGNMENT_PROCESS_INPUT_H
