#include "structs.h"
#include "process_input.h"
#include "power.h"
#include "windingnumber.h"

#define WAP_MAX 99
#define POINT_MAX 99
#define GRID_SIZE 78
#define MIN_DB -50

void stage_1(wap_t *waps, int wap_count);
void stage_2(wap_t waps[], int wap_count, point_t points[], int point_count);
void stage_3(wap_t waps[], int wap_count);
void stage_4(wap_t waps[], int wap_count);
void stage_5(wap_t waps[], int wap_count, point_t verts[], int vert_count);
void calculate_and_print_max_power(wap_t *waps, int wap_count, point_t point);
char map_character_for_value(double value);

int main(int argc, char **argv) {
    FILE *inputFile = NULL;
    wap_t waps[WAP_MAX];
    point_t points[POINT_MAX], vert_points[POINT_MAX];
    int wap_count = 0, point_count = 0, vert_count = 0;

    inputFile = stdin;
    if (argc > 1) {
        inputFile = fopen(argv[1], "r");
    }
    parse_input(inputFile, waps, points, vert_points, &wap_count, &point_count, &vert_count);
    stage_1(waps, wap_count);
    stage_2(waps, wap_count, points, point_count);
    stage_3(waps, wap_count);
    stage_4(waps, wap_count);
    stage_5(waps, wap_count, vert_points, vert_count);
    return 0;
}

void stage_1(wap_t *waps, int wap_count) {
    printf("Stage 1\n==========\n");
    printf("Number of WAPs: %02d\n", wap_count);
    point_t origin = {0, 0};
    calculate_and_print_max_power(waps, wap_count, origin);
}

void stage_2(wap_t waps[], int wap_count, point_t points[], int point_count) {
    printf("Stage 2\n==========\n");
    int i;
    for (i = 0; i < point_count; i++) {
        calculate_and_print_max_power(waps, wap_count, points[i]);
    }
}

void calculate_and_print_max_power(wap_t *waps, int wap_count, point_t point) {
    printf("Maximum signal strength at (%04.1f, %04.1f): %4.2f dBm\n", point.x, point.y,
           max_power_at_point(waps, wap_count, point));
}


void stage_3(wap_t waps[], int wap_count) {
    const int grid_start = 1;
    int i, j, sample_count = 0, poor_signal_count = 0;
    point_t point;

    for (i = grid_start; i < GRID_SIZE; i++) {
        point.x = i;
        for (j = grid_start; j < GRID_SIZE; j++) {
            point.y = j;
            sample_count++;
            poor_signal_count += (max_power_at_point(waps, wap_count, point) < MIN_DB) ? 1 : 0;
        }
    }
    double poor_signal_ratio = ((double) poor_signal_count) / sample_count * 100;
    printf("Stage 3\n==========\n");
    printf("%d points sampled\n", sample_count);
    printf("%d points (%4.2f%%) with maximum signal strength <= %d dBm\n", poor_signal_count, poor_signal_ratio,
           MIN_DB);
}

void stage_4(wap_t waps[], int wap_count) {
    int x, y;
    point_t point;
    char singleRow[GRID_SIZE + 1];
    singleRow[GRID_SIZE] = '\0';
    for (y = GRID_SIZE - 1; y > 0; y -= 2) {
        point.y = y;
        for (x = 0; x < GRID_SIZE; x++) {
            point.x = x + 0.5;
            singleRow[x] = map_character_for_value(max_power_at_point(waps, wap_count, point));
        }
        printf("%s\n", singleRow);
    }
}

void stage_5(wap_t waps[], int wap_count, point_t verts[], int vert_count) {
    printf("Stage 5\n==========\n");
    int x, y;
    point_t point;
    char singleRow[GRID_SIZE + 1];
    singleRow[GRID_SIZE] = '\0';
    for (y = GRID_SIZE - 1; y > 0; y -= 2) {
        point.y = y;
        for (x = 0; x < GRID_SIZE; x++) {
            point.x = x + 0.5;
            if ((crossing_number(point, verts, vert_count))) {
                singleRow[x] = map_character_for_value(max_power_at_point(waps, wap_count, point));
            } else {
                singleRow[x] = '#';
            }
        }
        printf("%s\n", singleRow);
    }
}

char map_character_for_value(double value) {
    if (value > 0) {
        return '+';
    } else if (value > -10) {
        return ' ';
    } else if (value > -20) {
        return '2';
    } else if (value > -30) {
        return ' ';
    } else if (value > -40) {
        return '4';
    } else if (value > -50) {
        return ' ';
    }
    return '-';
}