//
// Created by Gareth on 28/12/2016.
//
#include "power.h"

/**
 * Uses pythagoras's formula to calculate distance between two points. c^2 = a^2 + b^2. Returns c^2 for performance reasons.
 * @param point1
 * @param point2
 * @return
 */
double distance_between_squared(point_t point1, point_t point2) {
    return pow((point2.x - point1.x), 2) +
           pow((point2.y - point1.y), 2);
}

/**
 * will return the transmission power lost between a wap and a certain point.
 * @param point
 * @param wap
 * @return
 */
double received_power(point_t point, wap_t wap) {
    double distance2 = distance_between_squared(point, wap.location);

    double power_loss = 10 * log10(distance2) + 20 * log10(wap.frequency) + 32.45;
    return wap.power - power_loss;
}

/**
 * given a point on a grid, and a list of WAPs, it will return the maximum power the point receives from any given WAP on the list.
 * @param waps the list of waps
 * @param wap_count the number of waps
 * @param point the point struct
 * @return
 */
double max_power_at_point(const wap_t *waps, int wap_count, point_t point) {
    int i;
    double max_strength = -MAXFLOAT, signal_strength;
    for (i = 0; i < wap_count; i++) {
        if ((signal_strength = received_power(point, waps[i])) > max_strength) {
            max_strength = signal_strength;
        }
    }
    return max_strength;
}